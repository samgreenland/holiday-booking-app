require("./bootstrap");
import "bootstrap";
import Vue from "vue";
import Calendar from "v-calendar/lib/components/calendar.umd";
import DatePicker from "v-calendar/lib/components/date-picker.umd";
import { createInertiaApp, Link } from "@inertiajs/inertia-vue";
import ProfilePage from "./Pages/ProfilePage.vue";
import Layout from "./Pages/Shared/Layout.vue";
import AdminPage from "./Pages/AdminPage.vue";
import RequestForm from "./Pages/RequestForm.vue";
import Modal from "./Pages/components/Modal.vue";
import Donut from "vue-css-donut-chart";
import "vue-css-donut-chart/dist/vcdonut.css";

Vue.component("inertia-link", Link);
Vue.component("Layout", Layout);
Vue.component("modal", Modal);
Vue.component("calender", Calendar);
Vue.component("date-picker", DatePicker);

createInertiaApp({
    resolve: (name) => {
        const page = require(`./Pages/${name}`).default;
        page.layout = Layout;
        return page;
    },

    setup({ el, App, props, plugin }) {
        Vue.use(plugin);
        Vue.use(Donut);

        new Vue({
            render: (h) => h(App, props),
        }).$mount(el);
    },
});
