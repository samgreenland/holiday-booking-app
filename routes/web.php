<?php

use Inertia\Inertia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\HolidayController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render("HomePage");
});

Route::get('/profile', function () {
    return inertia('ProfilePage', ["name" => Auth::user()->name]);
})->middleware("auth");

Route::get('/calender', function () {
    return Inertia::render("CalenderPage", ["user" => Auth::user()]);
});

// Route::get('/admin', [AdminController::class, "index"])->middleware("auth", "can:isAdmin, App\Models\User");

Route::get('/admin', [AdminController::class, "index"])->middleware("auth");


Route::get("/request", function () {
    return
        Inertia::render("RequestForm");
});

Route::get("/my_holiday", function () {
    return
        Inertia::render("MyHolidayPage");
})->middleware("auth");

Route::get("/my_holiday/allowance", [HolidayController::class, 'show']);

Route::post("/request/store", [HolidayController::class, 'store']);

Route::get("/holiday", [HolidayController::class, 'index']);
Route::put("/holiday/approve", [HolidayController::class, 'edit']);
// Route::put("/holiday/reject", [HolidayController::class, 'edit']);

Route::get("/users", [UsersController::class, 'show']);
