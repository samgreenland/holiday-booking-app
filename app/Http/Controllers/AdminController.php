<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;


class AdminController extends Controller
{
    //
    public function index()
    {
        if (Gate::allows("is-admin")) {
            return Inertia::render("AdminPage");
        } else {
            return redirect("/");
        }
    }
}
